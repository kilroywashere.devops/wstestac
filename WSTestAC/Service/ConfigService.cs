namespace WSTestAC.Service;

public interface IConfigService
{
    string EnvironmentLaunch { get; set; }
}

public class ConfigService : IConfigService
{
    public string EnvironmentLaunch { get; set; }
    
    public ConfigService(string environmentLaunch)
    {
        EnvironmentLaunch = environmentLaunch;
    }
}