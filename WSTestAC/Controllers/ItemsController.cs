using Microsoft.AspNetCore.Mvc;
using WSTestAC.Models;
using WSTestAC.Service;

namespace WSTestAC.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ItemsController : ControllerBase
{
    private readonly ILogger<ItemsController> _logger;
    private readonly IConfigService _configService;

    public ItemsController(IConfigService configService, ILogger<ItemsController> logger)
    {
        // Gestion des logs
        _logger = logger;
        _configService = configService;
    }
    
    [HttpGet]
    public ActionResult<List<Item>> GetAll()
    {
        _logger.LogInformation("ItemsController.GetAll");
        return Enumerable.Range(1, 5).Select(index => new Item
        {
            Id = index,
            Description = $"Item{index}",
            Price = index * 100,

        }).ToList();
    }

    [HttpGet("{id}")]
    public ActionResult<Item> GetOne(int id)
    {
        _logger.LogInformation($"ItemsController.GetOne({id})");
        if (id < 1 || id > 5)
        {
            return NotFound();
        }
        
        return new Item
        {
            Id = id,
            Description = $"Item{id}",
            Price = id * 100,

        };
    }

    [HttpGet]
    [Route("config")]
    public ActionResult<string> GetConfig()
    {
        _logger.LogWarning("ItemsController.GetConfig");
        return _configService.EnvironmentLaunch;
    }
}